﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InvoiceCreator.Core.Models.CSVToObject
{
    class Supplier
    {
        public String PayzoneRef { get; set; }

        public String EmailAddress { get; set; }

        public static Supplier FromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            Supplier supplier = new Supplier();
            supplier.PayzoneRef = Convert.ToString(values[0]);
            supplier.EmailAddress = Convert.ToString(values[1]);
            return supplier;
        }
    }
}
