using System;
using System.ComponentModel;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Globalization;

namespace Core.Models
{
	
	public class InvoiceHeader
	{
		
		// ELEMENTS
		[XmlElement("InvoiceAccount")]
		public InvoiceAccount InvoiceAccount { get; set; }
		
		[XmlElement("InvoiceDate")]
		public InvoiceDate InvoiceDate { get; set; }
		
		[XmlElement("InvoiceNumber")]
		public InvoiceNumber InvoiceNumber { get; set; }
		
		[XmlElement("InvoicePeriod")]
		public InvoicePeriod InvoicePeriod { get; set; }
		
		[XmlElement("DeliveryMethod")]
		public DeliveryMethod DeliveryMethod { get; set; }
		
		[XmlElement("DeliveryAddress")]
		public DeliveryAddress DeliveryAddress { get; set; }
		
		[XmlElement("InvoiceIssuer")]
		public InvoiceIssuer InvoiceIssuer { get; set; }
		
		// CONSTRUCTOR
		public InvoiceHeader()
		{}
	}
}
