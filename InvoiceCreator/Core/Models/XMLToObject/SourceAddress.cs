using System;
using System.ComponentModel;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Globalization;

namespace Core.Models
{
	
	public class SourceAddress
	{
		
		// ELEMENTS
		[XmlElement("AddressLine1")]
		public SourceAddressAddressLine1 SourceAddressAddressLine1 { get; set; }
		
		[XmlElement("AddressLine2")]
		public SourceAddressAddressLine2 SourceAddressAddressLine2 { get; set; }
		
		[XmlElement("AddressLine3")]
		public AddressLine3 AddressLine3 { get; set; }
		
		[XmlElement("AddressLine4")]
		public SourceAddressAddressLine4 SourceAddressAddressLine4 { get; set; }
		
		[XmlElement("AddressLine5")]
		public SourceAddressAddressLine5 SourceAddressAddressLine5 { get; set; }
		
		[XmlElement("AddressLine6")]
		public AddressLine6 AddressLine6 { get; set; }
		
		// CONSTRUCTOR
		public SourceAddress()
		{}
	}
}
