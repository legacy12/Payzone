using System;
using System.ComponentModel;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Globalization;

namespace Core.Models
{
	
	public class DeliveryAddress
	{
		
		// ELEMENTS
		[XmlElement("ContactName")]
		public ContactName ContactName { get; set; }
		
		[XmlElement("AddressLine1")]
		public DeliveryAddressAddressLine1 DeliveryAddressAddressLine1 { get; set; }
		
		[XmlElement("AddressLine2")]
		public DeliveryAddressAddressLine2 DeliveryAddressAddressLine2 { get; set; }
		
		[XmlElement("AddressLine4")]
		public DeliveryAddressAddressLine4 DeliveryAddressAddressLine4 { get; set; }
		
		[XmlElement("AddressLine5")]
		public DeliveryAddressAddressLine5 DeliveryAddressAddressLine5 { get; set; }
		
		[XmlElement("AddressLine7")]
		public AddressLine7 AddressLine7 { get; set; }
		
		[XmlElement("EmailAddress")]
		public EmailAddress EmailAddress { get; set; }
		
		// CONSTRUCTOR
		public DeliveryAddress()
		{}
	}
}
