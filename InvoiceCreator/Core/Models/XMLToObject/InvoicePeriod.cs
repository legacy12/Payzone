using System;
using System.ComponentModel;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Globalization;

namespace Core.Models
{
	
	public class InvoicePeriod
	{
		
		// ELEMENTS
		[XmlElement("StartDate")]
		public StartDate StartDate { get; set; }
		
		[XmlElement("EndDate")]
		public EndDate EndDate { get; set; }
		
		// CONSTRUCTOR
		public InvoicePeriod()
		{}
	}
}
