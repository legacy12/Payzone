﻿using System.Xml.Serialization;

namespace Core.Models
{
    public class Product
    {
        // ELEMENTS
        [XmlText]
        public string Value { get; set; }

        // CONSTRUCTOR
        public Product()
        { }
    }
}