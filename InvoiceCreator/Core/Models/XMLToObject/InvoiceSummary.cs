using System;
using System.ComponentModel;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Globalization;

namespace Core.Models
{
	
	public class InvoiceSummary
	{
		
		// ELEMENTS
		[XmlElement("TotalAmount")]
		public TotalAmount TotalAmount { get; set; }
		
		[XmlElement("TotalVATAmount")]
		public TotalVATAmount TotalVATAmount { get; set; }
		
		[XmlElement("TotalBalance")]
		public TotalBalance TotalBalance { get; set; }
		
		// CONSTRUCTOR
		public InvoiceSummary()
		{}
	}
}
