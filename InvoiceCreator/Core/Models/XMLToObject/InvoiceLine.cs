using System;
using System.ComponentModel;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Globalization;

namespace Core.Models
{
	
	public class InvoiceLine
	{
		
		// ELEMENTS
		[XmlElement("Description")]
		public Description Description { get; set; }
		
		[XmlElement("FinalAmount")]
		public FinalAmount FinalAmount { get; set; }

        [XmlElement("BaseAmount")]
        public BaseAmount BaseAmount { get; set; }

        [XmlElement("VATCode")]
		public VATCode VATCode { get; set; }

        [XmlElement("Product")]
        public Product Product { get; set; }

        [XmlElement("TerminalType")]
        public TerminalType TerminalType { get; set; }

        [XmlElement("TerminalId")]
        public TerminalId TerminalId { get; set; }

        [XmlElement("StartDate")]
        public StartDate StartDate { get; set; }

        [XmlElement("EndDate")]
        public EndDate EndDate { get; set; }

        // CONSTRUCTOR
        public InvoiceLine()
		{}
	}
}
