﻿using System.Xml.Serialization;

namespace Core.Models
{
    public class BaseAmount
    {
        // ELEMENTS
        [XmlText]
        public decimal Value { get; set; }

        // CONSTRUCTOR
        public BaseAmount()
        { }
    }
}