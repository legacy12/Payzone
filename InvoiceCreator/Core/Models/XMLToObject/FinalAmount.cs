using System;
using System.ComponentModel;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Globalization;

namespace Core.Models
{
	
	public class FinalAmount
	{
		
		// ELEMENTS
		[XmlText]
		public decimal Value { get; set; }
		
		// CONSTRUCTOR
		public FinalAmount()
		{}
	}
}
