using System;
using System.ComponentModel;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Globalization;

namespace Core.Models
{
	
	public class Invoices
	{
		
		// ELEMENTS
		[XmlElement("Invoice")]
		public List<Invoice> Invoice { get; set; }
		
		// CONSTRUCTOR
		public Invoices()
		{}
	}
}
