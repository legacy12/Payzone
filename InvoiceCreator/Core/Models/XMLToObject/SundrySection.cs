using System;
using System.ComponentModel;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Globalization;

namespace Core.Models
{
	
	public class SundrySection
	{
		
		// ELEMENTS
		[XmlElement("InvoiceLine")]
		public List<InvoiceLine> InvoiceLine { get; set; }
		
		[XmlElement("SectionTotal")]
		public SectionTotal SectionTotal { get; set; }
		
		// CONSTRUCTOR
		public SundrySection()
		{}
	}
}
