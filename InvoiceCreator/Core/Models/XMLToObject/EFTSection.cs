﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace InvoiceCreator.Core.Models.XMLToObject
{
    public class EFTSection
    {

        // ELEMENTS
        [XmlElement("InvoiceLine")]
        public List<InvoiceLine> InvoiceLine { get; set; }

        [XmlElement("SectionTotal")]
        public SectionTotal SectionTotal { get; set; }

        // CONSTRUCTOR
        public EFTSection()
        { }
    }
}
