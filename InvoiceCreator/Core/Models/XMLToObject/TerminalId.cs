﻿using System.Xml.Serialization;

namespace Core.Models
{
    public class TerminalId
    {
        // ELEMENTS
        [XmlText]
        public string Value { get; set; }

        // CONSTRUCTOR
        public TerminalId()
        { }
    }
}