using System;
using System.ComponentModel;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Globalization;

namespace Core.Models
{
	
	public class InvoiceIssuer
	{
		
		// ELEMENTS
		[XmlElement("SourceAddress")]
		public SourceAddress SourceAddress { get; set; }
		
		[XmlElement("VATNumber")]
		public VATNumber VATNumber { get; set; }
		
		[XmlElement("CompanyNumber")]
		public CompanyNumber CompanyNumber { get; set; }
		
		[XmlElement("CompanyRegistered")]
		public CompanyRegistered CompanyRegistered { get; set; }
		
		// CONSTRUCTOR
		public InvoiceIssuer()
		{}
	}
}
