using System;
using System.ComponentModel;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Globalization;
using InvoiceCreator.Core.Models.XMLToObject;

namespace Core.Models
{
	
	public class Invoice
	{
		
		// ELEMENTS
		[XmlElement("InvoiceHeader")]
		public InvoiceHeader InvoiceHeader { get; set; }
		
		[XmlElement("SundrySection")]
		public SundrySection SundrySection { get; set; }

        [XmlElement("EFTSection")]
        public EFTSection EFTSection { get; set; }

        [XmlElement("InvoiceSummary")]
		public InvoiceSummary InvoiceSummary { get; set; }
		
		[XmlElement("VATRates")]
		public VATRates VATRates { get; set; }

        public Boolean UseEmail { get; set; }
		
		// CONSTRUCTOR
		public Invoice()
		{
            UseEmail = false;
        }
	}
}
