﻿using System.Xml.Serialization;

namespace Core.Models
{
    public class TerminalType
    {
        // ELEMENTS
        [XmlText]
        public string Value { get; set; }

        // CONSTRUCTOR
        public TerminalType()
        { }
    }
}