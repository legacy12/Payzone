﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InvoiceCreator.Core.Models.Email
{
    class EmailAddress
    {
        public string Name { get; set; }
        public string Address { get; set; }

        public EmailAddress(string name, string address)
        {
            this.Address = address;
            this.Name = name;
        }
    }
}
