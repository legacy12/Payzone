﻿using Core.Models;
using DinkToPdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceCreator.Helpers
{
    class HTMLToPDF
    {
        /*
        0 Address name
        1 Address premises
        2 Address street
        3 Address town
        4 Address country
        5 Address postcode
        6 Invoice date - generated
        7 Invoice number
        8 Invoice account
        9 Invoice period start
        10 Invoice period stop
        11 EFT section total
        12 Total balance - is with VAT
        13 VAT
        14 Invoice line 1 - EFT
        15 Invoice line 2 - EFT
        16 Invoice line 3 - EFT
        17 Invoice line 4 - EFT
        18 Invoice line 5 - EFT
        19 Invoice line 1 - Sundry
        20 Invoice line 2 - Sundry
        21 Invoice line 3 - Sundry
        22 Invoice line 4 - Sundry
        23 Invoice line 5 - Sundry
        24 Sundry section total
        25 Style
        26 Total amount
         */
        public String GenerateHTMLWithContents(string html, Invoice invoice)
        {
            //Address

            string name = "";
            if (invoice.InvoiceHeader.DeliveryAddress.ContactName != null)
            {
                name = invoice.InvoiceHeader.DeliveryAddress.ContactName.Value;
            }

            string premises = "";
            if(invoice.InvoiceHeader.DeliveryAddress.DeliveryAddressAddressLine1 != null)
            {
                premises = invoice.InvoiceHeader.DeliveryAddress.DeliveryAddressAddressLine1.Value;
            }

            string street = "";
            if (invoice.InvoiceHeader.DeliveryAddress.DeliveryAddressAddressLine2 != null)
            {
                street = invoice.InvoiceHeader.DeliveryAddress.DeliveryAddressAddressLine2.Value;
            }

            string town = "";
            if (invoice.InvoiceHeader.DeliveryAddress.DeliveryAddressAddressLine4 != null)
            {
                town = invoice.InvoiceHeader.DeliveryAddress.DeliveryAddressAddressLine4.Value;
            }

            string country = "";
            if (invoice.InvoiceHeader.DeliveryAddress.DeliveryAddressAddressLine5 != null)
            {
                country = invoice.InvoiceHeader.DeliveryAddress.DeliveryAddressAddressLine5.Value;
            }

            string postcode = "";
            if (invoice.InvoiceHeader.DeliveryAddress.AddressLine7 != null)
            {
                postcode = invoice.InvoiceHeader.DeliveryAddress.AddressLine7.Value;
            }

            

            //Invoice
            DateTime dateTime = DateTime.Today;
            string date = invoice.InvoiceHeader.InvoiceDate.Value;
            string Inumber = Convert.ToString(invoice.InvoiceHeader.InvoiceNumber.Value);
            string IAccount = invoice.InvoiceHeader.InvoiceAccount.Value;
            string IInvoiceStart = invoice.InvoiceHeader.InvoicePeriod.StartDate.Value;
            string IInvoiceEnd = invoice.InvoiceHeader.InvoicePeriod.EndDate.Value;
            string eftSectionTotal = "0.00";
            string sundrySectionTotal = "0.00";

            List<String> EFTInvoiceLines = new List<String>();
            List<String> SundryInvoiceLines = new List<String>();

            //EFT
            if (invoice.EFTSection != null)
            {
                for (int i = 0; i < invoice.EFTSection.InvoiceLine.Count; i++)
                {

                    if (invoice.EFTSection.SectionTotal != null)
                    {
                        eftSectionTotal = Convert.ToString(invoice.EFTSection.SectionTotal.Value);
                    }
                    else
                    {
                        eftSectionTotal = "";
                    }

                    if (invoice.EFTSection.InvoiceLine[i] != null)
                    {
                        string model;
                        string terminalType;
                        string terminalId;
                        string vatCode;
                        string charge;
                        string startDate;
                        string endDate;

                        if (invoice.EFTSection.InvoiceLine[i].Product != null)
                        {
                            model = invoice.EFTSection.InvoiceLine[i].Product.Value;
                        }
                        else
                        {
                            model = "";
                        }

                        if (invoice.EFTSection.InvoiceLine[i].TerminalType != null)
                        {
                            terminalType = invoice.EFTSection.InvoiceLine[i].TerminalType.Value;
                        }
                        else
                        {
                            terminalType = "";
                        }

                        if (invoice.EFTSection.InvoiceLine[i].TerminalId != null)
                        {
                            terminalId = invoice.EFTSection.InvoiceLine[i].TerminalId.Value;
                        }
                        else
                        {
                            terminalId = "";
                        }

                        if (invoice.EFTSection.InvoiceLine[i].VATCode != null)
                        {
                            vatCode = invoice.EFTSection.InvoiceLine[i].VATCode.Value;
                        }
                        else
                        {
                            vatCode = "";
                        }

                        if (invoice.EFTSection.InvoiceLine[i].StartDate != null)
                        {
                            startDate = invoice.EFTSection.InvoiceLine[i].StartDate.Value;
                        }
                        else
                        {
                            startDate = "";
                        }

                        if (invoice.EFTSection.InvoiceLine[i].EndDate != null)
                        {
                            endDate = invoice.EFTSection.InvoiceLine[i].EndDate.Value;
                        }
                        else
                        {
                            endDate = "";
                        }

                        charge = Convert.ToString(invoice.EFTSection.InvoiceLine[i].FinalAmount.Value);
                        EFTInvoiceLines.Add(AddInvoiceLine(model, terminalType, terminalId, startDate, endDate, vatCode, charge));
                    }
                }

            }

            //SUNDRYSECTION
            if (invoice.SundrySection != null)
            {
                for (int i = 0; i < invoice.SundrySection.InvoiceLine.Count; i++)
                {

                    if (invoice.SundrySection.SectionTotal != null)
                    {
                        sundrySectionTotal = Convert.ToString(invoice.SundrySection.SectionTotal.Value);
                    }
                    else
                    {
                        sundrySectionTotal = "";
                    }

                    if (invoice.SundrySection.InvoiceLine[i] != null)
                    {
                        string description;
                        string charge;
                        string startDate;
                        string endDate;

                        if (invoice.SundrySection.InvoiceLine[i].Description != null)
                        {
                            description = invoice.SundrySection.InvoiceLine[i].Description.Value;
                        }
                        else
                        {
                            description = "";
                        }

                        if (invoice.SundrySection.InvoiceLine[i].StartDate != null)
                        {
                            startDate = invoice.SundrySection.InvoiceLine[i].StartDate.Value;
                        }
                        else
                        {
                            startDate = "";
                        }

                        if (invoice.SundrySection.InvoiceLine[i].EndDate != null)
                        {
                            endDate = invoice.SundrySection.InvoiceLine[i].EndDate.Value;
                        }
                        else
                        {
                            endDate = "";
                        }

                        charge = Convert.ToString(invoice.SundrySection.InvoiceLine[i].FinalAmount.Value);
                        SundryInvoiceLines.Add(AddSundryInvoiceLine(description, charge, startDate, endDate));
                    }
                }
            }

            //Billing
            string InvoiceTotalAmount = Convert.ToString(invoice.InvoiceSummary.TotalAmount.Value);
            string InvoiceBalance = Convert.ToString(invoice.InvoiceSummary.TotalBalance.Value);
            string VAT = Convert.ToString(invoice.InvoiceSummary.TotalVATAmount.Value);
            string EFTComplete = "";

            switch (EFTInvoiceLines.Count)
             {
                case 0:
                    EFTComplete = string.Format(html, name, premises, street, town, country, postcode, date, Inumber,
                        IAccount, IInvoiceStart, IInvoiceEnd, eftSectionTotal, InvoiceBalance, VAT, "", "", "", "", "",
                        "{0}", "{1}", "{2}", "{3}", "{4}", "{5}", "{6}", InvoiceTotalAmount);
                    break;
                case 1:
                    EFTComplete = string.Format(html, name, premises, street, town, country, postcode, date, Inumber,
                        IAccount, IInvoiceStart, IInvoiceEnd, eftSectionTotal, InvoiceBalance, VAT, EFTInvoiceLines[0], "", "", "", "",
                        "{0}", "{1}", "{2}", "{3}", "{4}", "{5}", "{6}", InvoiceTotalAmount);
                    break;
                case 2:
                    EFTComplete = string.Format(html, name, premises, street, town, country, postcode, date, Inumber,
                        IAccount, IInvoiceStart, IInvoiceEnd, eftSectionTotal, InvoiceBalance, VAT, EFTInvoiceLines[0], EFTInvoiceLines[1], "", "", "",
                        "{0}", "{1}", "{2}", "{3}", "{4}", "{5}", "{6}", InvoiceTotalAmount);
                    break;
                case 3:
                    EFTComplete = string.Format(html, name, premises, street, town, country, postcode, date, Inumber,
                        IAccount, IInvoiceStart, IInvoiceEnd, eftSectionTotal, InvoiceBalance, VAT, EFTInvoiceLines[0], EFTInvoiceLines[1],
                        EFTInvoiceLines[2], "", "",
                        "{0}", "{1}", "{2}", "{3}", "{4}", "{5}", "{6}", InvoiceTotalAmount);
                    break;
                case 4:
                    EFTComplete = string.Format(html, name, premises, street, town, country, postcode, date, Inumber,
                        IAccount, IInvoiceStart, IInvoiceEnd, eftSectionTotal, InvoiceBalance, VAT, EFTInvoiceLines[0], EFTInvoiceLines[1],
                        EFTInvoiceLines[2], EFTInvoiceLines[3], "",
                        "{0}", "{1}", "{2}", "{3}", "{4}", "{5}", "{6}", InvoiceTotalAmount);
                    break;
                case 5:
                    EFTComplete = string.Format(html, name, premises, street, town, country, postcode, date, Inumber,
                        IAccount, IInvoiceStart, IInvoiceEnd, eftSectionTotal, InvoiceBalance, VAT, EFTInvoiceLines[0], EFTInvoiceLines[1],
                        EFTInvoiceLines[2], EFTInvoiceLines[3], EFTInvoiceLines[4],
                        "{0}", "{1}", "{2}", "{3}", "{4}", "{5}", "{6}", InvoiceTotalAmount);
                    break;
                default:
                    EFTComplete = string.Format(html, name, premises, street, town, country, postcode, date, Inumber,
                        IAccount, IInvoiceStart, IInvoiceEnd, eftSectionTotal, InvoiceBalance, VAT, "", "", "", "", "",
                        "{0}", "{1}", "{2}", "{3}", "{4}", "{5}", "{6}", InvoiceTotalAmount);
                    break;
            }

            switch(SundryInvoiceLines.Count) {
                case 0:
                    EFTComplete = string.Format(EFTComplete, "", "", "", "", "", sundrySectionTotal, GetStyle());
                    break;
                case 1:
                    EFTComplete = string.Format(EFTComplete, SundryInvoiceLines[0], "", "", "", "", sundrySectionTotal, GetStyle());
                    break;
                case 2:
                    EFTComplete = string.Format(EFTComplete, SundryInvoiceLines[0], SundryInvoiceLines[1], "", "", "", sundrySectionTotal, GetStyle());
                    break;
                case 3:
                    EFTComplete = string.Format(EFTComplete, SundryInvoiceLines[0], SundryInvoiceLines[1], SundryInvoiceLines[2], "", "", sundrySectionTotal, GetStyle());
                    break;
                case 4:
                    EFTComplete = string.Format(EFTComplete, SundryInvoiceLines[0], SundryInvoiceLines[1], SundryInvoiceLines[2], SundryInvoiceLines[3], "", sundrySectionTotal, GetStyle());
                    break;
                case 5:
                    EFTComplete = string.Format(EFTComplete, SundryInvoiceLines[0], SundryInvoiceLines[1], SundryInvoiceLines[2], SundryInvoiceLines[3], SundryInvoiceLines[4], sundrySectionTotal, GetStyle());
                    break;
                default:
                    EFTComplete = string.Format(EFTComplete, "", "", "", "", "", sundrySectionTotal, GetStyle());
                    break;
            }



            return EFTComplete;
        }

        public void ToPDF(BasicConverter converter, string contents, string output)
        {
            var doc = new HtmlToPdfDocument()
            {
                GlobalSettings = {
                    ColorMode = ColorMode.Color,
                    Orientation = Orientation.Portrait,
                    PaperSize = PaperKind.A3,
                    Out = @output
                },
                Objects = {
                    new ObjectSettings() {
                        UseExternalLinks = true,
                        PagesCount = true,
                        HtmlContent = @contents,
                        WebSettings = { DefaultEncoding = "utf-8" },
                    }
                }
            };

            converter.Convert(doc);
        }




        /* 
        <tr>
            <td class='model'>{0}</td> 
            <td class='terminaltype'>{1}</td> 
            <td class='terminalid'>{2}</td> 
            <td class='chargeperiod'>{3} to {4}</td> 
            <td class='vatcode'>{5}</td> 
            <td class='charge'>{6}</td> 
        </tr>
         */

        public string AddInvoiceLine(string model, string terminalType, string terminalId, string chargeStart, string chargeEnd,
            string vatCode, string charge)
        {
            string invoiceLineBlank = "<tr><td class='model'>{0}</td><td class='terminaltype'>{1}</td><td class='terminalid'>{2}</td>" +
                            "<td class='chargeperiod'>{3} to {4}</td><td class='vatcode'>{5}</td><td class='charge'>{6}</td></tr>";

            return string.Format(invoiceLineBlank, model, terminalType, terminalId, chargeStart, chargeEnd, vatCode, charge);
        }

        /* 
        <tr>
            <td class='description'>{0}</td> 
            <td class='chargeperiod'>{1} to {2}</td> 
            <td class='charge'>{3}</td> 
        </tr>
         */

        public string AddSundryInvoiceLine(string description, string charge, string chargeStart, string chargeEnd)
        {
            string invoiceLineBlank = "<tr><td class='description'>{0}</td><td class='chargeperiod'>{1} to {2}</td> <td class='charge'>{3}</td></tr>";

            return string.Format(invoiceLineBlank, description, chargeStart, chargeEnd, charge);
        }

        //public string AddSundryTable()
        //{
        //    return  "<div class='row'><div class='col-6'><table class='invoice-table'><tr class='headline'><th colspan = '5' >Sundry Charges...<hr></th>" +
        //                     "<th>&nbsp;<hr></th></tr><tr class='table-header'><th>DESCRIPTION</th><th>VALUE</th><th>CHARGE PERIOD</th></tr>{20}{21}{22}{23}{24}" +
        //                     "<tr class='headline'><th colspan ='5'><hr> Sub Total:</th><th><hr>£{11}</th></tr></table></div></div>";
        //}


        public string GetStyle()
        {
            return "<style>@import url(https://fonts.googleapis.com/css?family=Lato:400,300,300italic,400italic,700,700italic);body,html{height:100%;width:100%;margin:0;padding:0;left:0;top:0;font-size:100%}.center,.container{margin-left:auto;margin-right:auto}*{font-family:Lato,Helvetica,sans-serif;color:#333447}h1{font-size:2.5rem}h2{font-size:2rem;margin-top:0px;margin-bottom:0px}h3{font-size:1.375rem}h4{font-size:1.125rem}h5{font-size:1rem}h6{font-size: .875rem}p{font-size:1.125rem;font-weight:200;margin-top:10px;margin-bottom:10px}.font-light{font-weight:300}.font-regular{font-weight:400}.font-heavy{font-weight:700}.left{text-align:left}.right{text-align:right}.center{text-align:center}.justify{text-align:justify}.container{width:90%}.row{position:relative;width:100%}.row [class^=col]{float:left;margin: .5rem 2%;min-height: .125rem}.col-1,.col-10,.col-11,.col-12,.col-2,.col-3,.col-4,.col-5,.col-6,.col-7,.col-8,.col-9{width:96%}.col-1-sm{width:4.33%}.col-2-sm{width:12.66%}.col-3-sm{width:21%}.col-4-sm{width:29.33%}.col-5-sm{width:37.66%}.col-6-sm{width:46%}.col-7-sm{width:54.33%}.col-8-sm{width:62.66%}.col-9-sm{width:71%}.col-10-sm{width:79.33%}.col-11-sm{width:87.66%}.col-12-sm{width:96%}.row::after{content:'';display:table;clear:both}.hidden-sm{display:none}@media only screen and (min-width:33.75em){.container{width:80%}}@media only screen and (min-width:45em){.col-1{width:4.33%}.col-2{width:12.66%}.col-3{width:21%}.col-4{width:29.33%}.col-5{width:37.66%}.col-6{width:46%}.col-7{width:54.33%}.col-8{width:62.66%}.col-9{width:71%}.col-10{width:79.33%}.col-11{width:87.66%}.col-12{width:96%}.hidden-sm{display:block}}@media only screen and (min-width:60em){.container{width:75%;max-width:60rem}}.logo>img{max-width:250px}.top-number{font-weight:bold;font-size:1.125rem}.details-container{margin-top:75px}.details-container>.address>p{margin-top:0px;margin-bottom:0px}.details-container>.info>p{margin-top:0px;margin-bottom:0px}.main{border:1px solid black;min-height:1200px;margin-top:25px !important}.header{margin-top:25px}.invoice-breakdown h1{margin-bottom:0}.invoice-table{width:100%;border:1px solid black;border-radius:10px}.headline{width:100%}.headline th{font-size:18px}th{text-align:left}th,td{padding:2px 5px}.footer{}hr{border:0;border-bottom:4px dotted #434343}@media screen and (max-width:520px){.mobile-table th, .mobile-table td{display:block}.table-header{display:none}.model::before{content:'MODEL: ';font-weight:bold}.description::before{content:'DESCRIPTION:';font-weight:bold;}.terminaltype::before{content:'TERMINAL TYPE: ';font-weight:bold}.terminalid::before{content:'TERMINAL ID: ';font-weight:bold}.chargeperiod::before{content:'CHARGE PERIOD: ';font-weight:bold}.vatcode::before{content:'VAT CODE: ';font-weight:bold}.charge::before{content:'CHARGE: ';font-weight:bold}.charge{margin-bottom:15px}.div-wrapper{position:relative;padding-right:25px;height:200px;width:600px}.div-wrapper img{position:absolute;right:0;bottom:0}}</style>";
        }
    }
}		  