﻿using InvoiceCreator.Core.Models.CSVToObject;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace InvoiceCreator.Helpers
{
    class CSVToObject
    {
        internal List<Supplier> ToObject(string csvLocation) {

            List<Supplier> values = File.ReadAllLines(csvLocation)
                                           .Skip(1)
                                           .Select(v => Supplier.FromCsv(v))
                                           .ToList();

            return values;
        }
    }
}
