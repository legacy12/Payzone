﻿using InvoiceCreator.Core.Models.Email;
using MailKit.Net.Smtp;
using MimeKit;
using MimeKit.Text;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace InvoiceCreator.Helpers
{
    class EmailSender
    {
        static string _SMTPServer;
        static int _SMTPPort;
        static string _SMTPUsername;
        static string _SMTPPassword;

        public EmailSender(string SMTPServer, int SMTPPort, string SMTPUsername, string SMTPpassword)
        {
            _SMTPServer = SMTPServer;
            _SMTPPort = SMTPPort;
            _SMTPUsername = SMTPUsername;
            _SMTPPassword = SMTPpassword;
        }

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public void SendEmail(EmailMessage emailMessage)
        {
            try
            {
                var builder = new BodyBuilder();
                string html = emailMessage.Content;

                var message = new MimeMessage();
                message.To.AddRange(emailMessage.ToAddresses.Select(x => new MailboxAddress(x.Name, x.Address)));
                message.From.AddRange(emailMessage.FromAddresses.Select(x => new MailboxAddress(x.Name, x.Address)));

                builder.HtmlBody = string.Format(html);
                builder.Attachments.Add(emailMessage.AttachmentLocation);

                message.Subject = emailMessage.Subject;
                message.Body = builder.ToMessageBody();

                //Be careful that the SmtpClient class is the one from Mailkit not the framework!
                using (var emailClient = new SmtpClient())
                {
                    //The last parameter here is to use SSL (Which you should!)
                    emailClient.Connect(_SMTPServer, _SMTPPort, false);

                    //Remove any OAuth functionality as we won't be using it. 
                    emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                    //emailClient.Authenticate(_SMTPUsername, _SMTPPassword);

                    emailClient.Send(message);

                    emailClient.Disconnect(true);
                }
            } catch(Exception e)
            {
                log.Error("Error sending email: " + e);
            }

        }

        public void SendBulk(List<EmailMessage> emailMessages)
        {
            //Be careful that the SmtpClient class is the one from Mailkit not the framework!
            using (var emailClient = new SmtpClient())
            {
                //The last parameter here is to use SSL (Which you should!)
                emailClient.Connect(_SMTPServer, _SMTPPort, false);

                //Remove any OAuth functionality as we won't be using it. 
                emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                //emailClient.Authenticate(_SMTPUsername, _SMTPPassword);

                foreach(EmailMessage emailMessage in emailMessages)
                {
                    var builder = new BodyBuilder();
                    string html = emailMessage.Content;

                    var message = new MimeMessage();
                    message.To.AddRange(emailMessage.ToAddresses.Select(x => new MailboxAddress(x.Name, x.Address)));
                    message.From.AddRange(emailMessage.FromAddresses.Select(x => new MailboxAddress(x.Name, x.Address)));

                    builder.HtmlBody = string.Format(html);
                    builder.Attachments.Add(emailMessage.AttachmentLocation);

                    message.Subject = emailMessage.Subject;
                    message.Body = builder.ToMessageBody();

                    emailClient.Send(message);
                    log.Debug("email sent, ref: " + emailMessage.Reference);
                }

                emailClient.Disconnect(true);
            }
        }
    }
}