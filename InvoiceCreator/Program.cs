﻿using Core.Models;
using DinkToPdf;
using InvoiceCreator.Core.Models.CSVToObject;
using InvoiceCreator.Core.Models.Email;
using InvoiceCreator.Helpers;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Reflection.Metadata;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace InvoiceCreator
{
    class Program
    {
        static EmailSender _emailSender;

        static IEnumerable<Invoice> emailSuppliers = new List<Invoice>();
        static IEnumerable<Invoice> mailSuppliers = new List<Invoice>();
        static List<EmailMessage> emailMessages = new List<EmailMessage>();

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        static void Main(string[] args)
        {
            BasicConverter converter = new BasicConverter(new PdfTools());

            //Log4Net setup
            XmlDocument log4netConfig = new XmlDocument();
            log4netConfig.Load(File.OpenRead("log4net.config"));
            var repo = log4net.LogManager.CreateRepository(Assembly.GetEntryAssembly(), typeof(log4net.Repository.Hierarchy.Hierarchy));
            log4net.Config.XmlConfigurator.Configure(repo, log4netConfig["log4net"]);

            //appsettings.json setup
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            IConfigurationRoot configuration = builder.Build();

            var doc = XDocument.Load(configuration["XMLFileLocation"]);
            string xmlcontents = doc.ToString();

            var html = File.ReadAllText(configuration["HTMLTemplateFileLocation"]);
            string htmlcontents = html.ToString();

            var emailBody = File.ReadAllText(configuration["EmailBodyLocation"]);
            string emailBodyContents = emailBody.ToString();

            var xmlToObject = new XmlToObject();
            var csvToObject = new CSVToObject();
            var htmlToPDF = new HTMLToPDF();

            //Email Sender properties
            string smtpServer = configuration["SmtpServer"];
            int smtpPort = Convert.ToInt32(configuration["SmtpPort"]);
            string smtpUsername = configuration["smtpUsername"];
            string smtpPassword = configuration["smtpPassword"];


            //Email settings
            Boolean useEmail = Convert.ToBoolean(configuration["UseEmail"]);
            Boolean emailOnly = Convert.ToBoolean(configuration["EmailOnly"]);
            Boolean printOnly = Convert.ToBoolean(configuration["GeneratePrintOnly"]);

            log.Debug(useEmail);
            log.Debug(emailOnly);
            log.Debug(printOnly);

            _emailSender = new EmailSender(smtpServer, smtpPort, smtpUsername, smtpPassword);

            Invoices invoices = (Invoices)xmlToObject.ToObject(xmlcontents, typeof(Invoices));

            List<Supplier> suppliers = csvToObject.ToObject(configuration["CSVFileLocation"]);

            Boolean correctSplit = SplitInvoices(invoices, suppliers);

            if(!emailOnly)
            {
                if (correctSplit)
                {
                    //Creates email invoices 
                    if (!printOnly)
                    {
                        for (int i = 0; i < emailSuppliers.Count(); i++)
                        {
                            log.Info("Working on email ref: " + emailSuppliers.ElementAt(i).InvoiceHeader.InvoiceAccount.Value);

                            string generatedHtml = htmlToPDF.GenerateHTMLWithContents(htmlcontents, emailSuppliers.ElementAt(i));

                            string outputLocation = configuration["EmailOutputDirectory"] +
                                emailSuppliers.ElementAt(i).InvoiceHeader.InvoiceAccount.Value + ".pdf";

                            htmlToPDF.ToPDF(converter, generatedHtml, outputLocation);

                            if (useEmail)
                            {
                                Supplier supplier = suppliers.Find(s => s.PayzoneRef == emailSuppliers.ElementAt(i).InvoiceHeader.InvoiceAccount.Value);
                                if (supplier != null)
                                {
                                    EmailMessage message = GenerateEmailMessage(emailSuppliers.ElementAt(i), emailBodyContents, outputLocation, supplier);
                                    _emailSender.SendEmail(message);
                                    log.Debug("Email sent to : " + supplier.EmailAddress);

                                    if (File.Exists(outputLocation))
                                    {
                                        File.Delete(outputLocation);
                                    }
                                }
                                else
                                {
                                    log.Error("Null supplier");
                                }
                            }
                        }

                        //if (useEmail)
                        //{
                        //    SendEmails();
                        //}

                        log.Debug("Emails completed. Number of emails sent: " + emailSuppliers.Count());

                    }

                    //Creates print invoices
                    for (int i = 0; i < mailSuppliers.Count(); i++)
                    {
                        try
                        {
                            string generatedHtml = htmlToPDF.GenerateHTMLWithContents(htmlcontents, mailSuppliers.ElementAt(i));

                            string outputLocation = configuration["PrintOutputDirectory"] +
                                mailSuppliers.ElementAt(i).InvoiceHeader.InvoiceAccount.Value + ".pdf";

                            htmlToPDF.ToPDF(converter, generatedHtml, outputLocation);

                            log.Info("Invoices done: " + (i));

                        }
                        catch (Exception e)
                        {
                            log.Error("Error occurred when processing invocing: " + mailSuppliers.ElementAt(i).ToString());
                            log.Error(e);
                        }
                        //log.Info("Working on print ref: " + mailSuppliers.ElementAt(i).InvoiceHeader.InvoiceAccount.Value);
                    }
                }
                else
                {
                    log.Error("Email split did not complete correctly : uneven split");
                    // TIME FOR ERROR
                }
            }
            else
            {
                //EMAIL ONlY - so files already generated
                for (int i = 0; i < emailSuppliers.Count(); i++)
                {
                string outputLocation = configuration["PrintOutputDirectory"] +
                    mailSuppliers.ElementAt(i).InvoiceHeader.InvoiceAccount.Value + ".pdf";

                    Supplier supplier = suppliers.Find(s => s.PayzoneRef == emailSuppliers.ElementAt(i).InvoiceHeader.InvoiceAccount.Value);
                    if (supplier != null)
                    {
                        GenerateEmail(emailSuppliers.ElementAt(i), emailBodyContents, outputLocation, supplier);
                    }
                }

                SendEmails();
            }
        }

        public static Boolean SplitInvoices(Invoices invoices, List<Supplier> suppliers) 
        {
            for (int i = 0; i < suppliers.Count; i++)
            {
                //TODO Catch error if more than one reference
                try
                {
                    Invoice invoice = invoices.Invoice.Where(iv => iv.InvoiceHeader.InvoiceAccount.Value == suppliers[i].PayzoneRef).SingleOrDefault();
                    if (invoice != null)
                    {
                        invoice.UseEmail = true;
                        var index = invoices.Invoice.FindIndex(inv => inv.InvoiceHeader.InvoiceAccount.Value == invoice.InvoiceHeader.InvoiceAccount.Value);
                        invoices.Invoice[index] = invoice;
                    }
                    else
                    {
                        log.Error("Payzone ref doesn't exist: " + suppliers[i].PayzoneRef);
                    }
                } catch (Exception e)
                {
                    log.Error("Duplicate invoice for : " + suppliers[i].PayzoneRef);
                }

            }

            log.Info("Number of total invoices: " + invoices.Invoice.Count());

            // Gets email invoices
            emailSuppliers = invoices.Invoice.Where(iv => iv.UseEmail == true);
            log.Info("Number of email invoices: " + emailSuppliers.Count());
            // Gets print invoices
            mailSuppliers = invoices.Invoice.Where(iv => iv.UseEmail == false);

            log.Info("Number of empty invoices: " + invoices.Invoice.Where(iv => iv.InvoiceSummary.TotalBalance.Value == 0).Count());
            log.Info("Number of mail invoices: " + mailSuppliers.Count());

            IEnumerable<Invoice> nonNullEFT = invoices.Invoice.Where(iv => iv.EFTSection != null);

            IEnumerable<Invoice> nonNullSundry = invoices.Invoice.Where(iv => iv.SundrySection != null);
            int maxLengthSundry = nonNullSundry.Max(x => x.SundrySection.InvoiceLine.Count);

            int maxLengthEFT = nonNullSundry.Max(x => x.SundrySection.InvoiceLine.Count);


            log.Info("Number of populated sundry: " + nonNullSundry.Count());
            log.Info("Maximum sundry items: " + maxLengthSundry);

            log.Info("Number of populated EFT: " + nonNullEFT.Count());
            log.Info("Maximum EFT items: " + maxLengthEFT);

            if (maxLengthEFT > 5 || maxLengthSundry > 5)
            {
                throw new Exception("Too many invoice lines. Update HTML template");
            }

            return emailSuppliers.Count() + mailSuppliers.Count() == invoices.Invoice.Count();

        }

        public static void GenerateEmail(Invoice invoice, String emailBody, String pdfLocation, Supplier supplier)
        {
            var emailMessage = new EmailMessage();
            emailMessage.ToAddresses.Add(new Core.Models.Email.EmailAddress("Invoicee", supplier.EmailAddress));
            emailMessage.FromAddresses.Add(new Core.Models.Email.EmailAddress("Payzone", "invoice@payzone.co.uk")); // TODO Externalise
            emailMessage.Subject = "Payzone Invoice";
            emailMessage.Content = emailBody;
            emailMessage.AttachmentLocation = pdfLocation;
            emailMessage.Reference = invoice.InvoiceHeader.InvoiceAccount.Value;

            emailMessages.Add(emailMessage);
            log.Debug("email generated for: " + invoice.InvoiceHeader.InvoiceAccount.Value);
        }

        public static EmailMessage GenerateEmailMessage(Invoice invoice, String emailBody, String pdfLocation, Supplier supplier)
        {
            var emailMessage = new EmailMessage();
            emailMessage.ToAddresses.Add(new Core.Models.Email.EmailAddress("Invoicee", supplier.EmailAddress));
            emailMessage.FromAddresses.Add(new Core.Models.Email.EmailAddress("Payzone", "invoice@payzone.co.uk")); // TODO Externalise
            emailMessage.Subject = "Payzone Invoice";
            emailMessage.Content = emailBody;
            emailMessage.AttachmentLocation = pdfLocation;
            emailMessage.Reference = invoice.InvoiceHeader.InvoiceAccount.Value;

            log.Debug("email generated for: " + invoice.InvoiceHeader.InvoiceAccount.Value);
            return emailMessage;
        }

        public static void SendEmails()
        {
            _emailSender.SendBulk(emailMessages);
            log.Debug("Emails sent!");
        }
    }
}
